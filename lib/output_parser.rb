require 'date'

class OutputParser

  def process
    results = {}

    cur_path = File.expand_path(File.dirname(__FILE__))
    File.open(File.join(cur_path, '../scripts/puma_mem_check.output')) do |f|
      while(l = f.gets)
        process_line(results, l)
      end
    end

    # get the average memory for each date
    results.map { |d, arr| [format_date(d), (arr.reduce(:+) / arr.length)] }.
      # populate results with header used for data visualization
      unshift(['Time', 'Mem Size (MB)'])
  end

  private

  def process_line(results, str)
    return if delimiter?(str)
    return (results[@cur_date = DateTime.rfc3339(str)] = []) if date?(str)
    # otherwise we have memory we want to capture
    # divide by 1024 since we MB and not KB
    results[@cur_date] << (str.split[1].to_i / 1024)
  end

  def delimiter?(str)
    str =~ /^-+$/
  end

  def date?(str)
    str =~ /^\d{4}-\d{2}-\d{2}/
  end

  def format_date(date)
    date.strftime('%m/%d - %H:%M')
  end

end
