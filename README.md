## Purpose
Based on the output from a simple bash script run via cron (see scripts dir), this app will display a visualization of memory usage of a *nix process over time.

![alt tag](https://raw.github.com/ckuttruff/memory_visualizer/master/screenshot.png)

## Using
1. Throw the example executable/crontab onto your server (obviously replacing with whatever process you care about :)
2. Wait some amount of time
3. scp the output file to the scripts dir of this repo
4. `ruby app.rb` :cake: :beer:

## Acknowledgements
All props for pretty graph and ease of use getting that displayed goes to this nifty project: https://developers.google.com/chart/

Highly recommend it.

## License
MIT/X11 (See LICENSE)
